# N3: Nathan's Neural Networks

## Report and Generative Network Output

Please see docs/final-report.pdf and docs/generative-output.txt, respectively.

## How to Run

You will need an implementation of ANSI Common Lisp. Any should do, but I suggest [SBCL](http://www.sbcl.org/).
Once installed, start your Lisp into a read-eval-print loop (interactive mode) and run:

```lisp
(load "load.lisp")
```

This will load systems `n3` and `n3/demos/sinewave`. To run the sinewave demo:

```lisp
;; To save us prefixing symbols with `n3/demos/sinewave:`.
(in-package :n3/demos/sinewave)
;; Then evaluate one or both of the following:
(train)
(run 30) ; Loads networks/sinewave.lisp, so training first isn't required.
```

The directory 'networks' contains some example neural networks. Here is an example of loading and using one:

```lisp
(setf *net* (load-net "networks/andgate-5.lisp"))
(run-net *net* 1 1 0 1) ; Or whichever four inputs you want.
```

For more examples of ways to use this system, please see <docs/Examples.md>.
