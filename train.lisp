(defpackage :n3/train
  (:use :cl :n3/utils :n3/network)
  (:export :*learn-rate* :*target-error* :end-epoch
           :epoch :derive-sigmoid
           :learn-once :learn-epoch :test-net :train-to-target))

(in-package :n3/train)

(defparameter *learn-rate* 0.5d0)
(defparameter *target-error* 0.005d0)

(declaim (type double-float *learn-rate*)
         (type double-float *target-error*))

(define-condition end-epoch (condition) ())

(defun epoch (data-src n)
  (let ((left n))
    (lambda ()
      (cond
        ((plusp left)
          (decf left)
          (funcall data-src))
        (t
          (setf left n)
          (error 'end-epoch))))))

(defun derive-sigmoid (sig-x)
  (* sig-x (- 1 sig-x)))

(defun learn-once (net expected inputs
                   &key (evaled-net (eval-net net)))
  (declare (optimize (speed 3)))
  (flet ((calc-deltas (layer-outputs last-deltas)
           (mapcar #'*
                   last-deltas
                   (mapcar #'derive-sigmoid layer-outputs)))

         (calc-next-deltas (layer deltas)
           (mapcar
             (lambda (delta-weights) (reduce #'+ delta-weights)) ; Sum each sublist
             (apply #'mapcar #'list ; Transpose to iterate w1 of all nodes, w2 of all nodes, ...
                    (mapcar (lambda (node old-delta)
                              (if (eq (car node) 'neuron)
                                (mapcar (lambda (weight) (* weight old-delta))
                                        (neuron-get-weights node))
                                (make-list-of (length inputs) old-delta)))
                            layer deltas))))

         (adjust-weights (layer inputs deltas)
           (mapc (lambda (node delta)
                   (declare (type double-float delta))
                   (when (eq (car node) 'neuron)
                     (mapl (lambda (weights inputs) ; Iterate by sublists so we can modify in-place.
                             (decf (car weights)
                                   (* *learn-rate*
                                      delta
                                      (the double-float (car inputs)))))
                           (neuron-get-weights node)
                           inputs)))
                 layer deltas)))

    (if net
      (let* ((layer-outputs (run-layer (car evaled-net) inputs))
             (recurse-results
               (multiple-value-list
                 (learn-once (cdr net)
                             expected
                             layer-outputs
                             :evaled-net (cdr evaled-net))))
             (deltas (calc-deltas layer-outputs (second recurse-results))))
        (adjust-weights (car net) inputs deltas)
        (values
          (first recurse-results)
          (calc-next-deltas (car net) deltas)))
      ;; Base case: recursed past the last layer.
      (values
        inputs
        (mapcar #'- inputs expected)))))

(defun learn-epoch (net data-src
                    &key (evaled-net (eval-net net)))
  (loop
    (let ((row (handler-case (funcall data-src)
                 (end-epoch () (return)))))
      (learn-once net (car row) (cdr row)
                  :evaled-net evaled-net))))

(defun test-net (net data-src)
  (let ((errors '()))
    (loop (let ((row (handler-case (funcall data-src)
                       (end-epoch () (return (mean errors))))))
            (mapc (lambda (expected result)
                    (push (abs (- expected result)) errors))
                  (car row)
                  (multiple-value-list (run-net-list net (cdr row))))))))

(defun train-to-target (net learn-src test-src)
  (let ((evaled (eval-net net)))
    (do ((epochs 0 (1+ epochs)))
      ((let ((err (progn ; This whole `let` form is the stop condition.
                    (format t "Now testing...~%")
                    (test-net net test-src))))
         (format t "Epochs: ~s, Mean error: ~s~%" epochs err)
         (<= err *target-error*)))
      (format t "Now learning...~%")
      (learn-epoch net learn-src
                   :evaled-net evaled)))
  (format t "Target error of ~s reached~%" *target-error*))
