(defpackage :n3/network
  (:use :cl :n3/utils)
  (:export :wsum :sigmoid :neuron-get-weights :net-inputs ; Network utilities
           :defnode :input :neuron :subnet :inspect-node ; Node types
           :make-neuron :make-layer :make-net :from-spec :chain ; Network building
           :save-net :load-net ; Persistence
           :eval-net :run-node :run-layer :run-net :run-net-list :run-net-generative)) ; Execution

(in-package :n3/network)

;;; Network utilities

(defun wsum (weights inputs)
  (apply #'+ (mapcar #'* weights inputs)))

(defun sigmoid (x)
  "Calculates 1/(1 + e^(-x)), also known as the logistic function."
  (/ 1 (+ 1 (exp (- x)))))

(defun neuron-get-weights (neuron)
  (second (second neuron)))

(defun net-inputs (net)
  "Attempts to return the number of inputs for a network whose
   first node is (INPUT N). Raises an error if this is not the
   first node in the network."
  (assert (eq 'input (caaar net)) ; The kids playing street hockey when someone drives by.
          (net)
          (format nil "Network~%  ~s~%does not begin with~%  ~s~%"
                  net '(input n)))
  (cadaar net))

;;; Node types

(defmacro defnode (name (&rest args) inputs-var &body body)
"Produces a function that returns a lambda.
`args` is bound when the node is created, before the network runs.
`inputs-var` will be passed in when the node is run. "
  `(defun ,name ,args
     ,(when (stringp (car body)) (pop body)) ; Include documentation outside the lambda.
     (lambda (&rest ,inputs-var)
       ,@body)))

(defnode input (expected-count) inputs
"Asserts that the number of input values is equal to expected.
Returns `inputs` unmodified if succeeded."
  (assert (= expected-count (length inputs))
            (expected-count inputs))
    (values-list inputs))

(defnode neuron (weights) inputs
  (sigmoid (wsum weights inputs)))

(defnode subnet (net) inputs
  (run-net-list net inputs))

(defnode inspect-node (&optional (node 'identity) label) inputs
  (let ((predicted
          (multiple-value-list (run-node node inputs))))
    (when label (format t "~A: " label))
    (unless (eq node 'identity) ; Print inputs. Skip if `node` is `identity`.
      (format t "~{~A ~}" inputs)) ; Use iteration block to avoid printing outermost parens.
    (format t "→ ~{~A ~}~%" predicted) ; Print predicted
    (values-list predicted)))

;;; Network building

(defun make-neuron (num-inputs)
  `(neuron ',(make-list-of num-inputs (- (random 2d0) 1d0))))

(defun make-layer (nodes-last nodes)
  (make-list-of nodes (make-neuron nodes-last)))

(defun make-net (layer-specs)
  (cons
    `((input ,(car layer-specs))) ; Input layer
    (loop for (nodes-last nodes-cur) on layer-specs
              while nodes-cur
              collecting (make-layer nodes-last nodes-cur))))

(defun from-spec ()
  (make-net (read-csv-line :prompt "Define your network: ")))

(defun chain (&rest lists)
  (apply #'concatenate 'list lists))

;;; Persistence

(defun save-net (net path)
  (with-open-file (file path :direction :output
                             :if-does-not-exist :create)
    (format file "~s~%" net)
    (values)))

(defun load-net (path)
  (with-open-file (file path :direction :input)
    (let ((*read-eval* nil)) (read file))))

;;; Execution

(defun eval-net (net)
  "Returns a new network where each node is the result of evaluating
   a node in `net`. Useful if you plan to repeatedly run the network
   without modification, such as during testing."
   (mapcar
     (lambda (layer) (mapcar #'eval layer))
     net))

(defun run-node (node inputs)
  (apply
    (if (listp node) (eval node) node) ; If the node is a Lisp form, evaluate it first.
    inputs))

(defun run-layer (layer inputs)
  (multimap
    (lambda (node) (run-node node inputs))
    layer))

(defun run-net (net &rest inputs) ; Making this variadic will be useful later.
  (if net
    (apply 'run-net
      (cdr net)
      (run-layer (car net) inputs))
    (values-list inputs))) ; Base case: no layers left

(defun run-net-list (net inputs)
  (apply 'run-net net inputs))

(defun run-net-generative (net data-src
                           &optional iterations
                                     number-formatter
                                     (num-inputs (net-inputs net))) ; Try to fetch automatically
  (let* ((net (eval-net net))
         (inputs (make-list-of num-inputs (funcall data-src)))
         (tail (last inputs)))
    (macrolet ((body (iter-number)
                 `(let* ((predicted
                           (multiple-value-list
                             (run-net-list net inputs)))
                         (actual
                           (make-list-of (length predicted) (funcall data-src)))
                         (errors
                           (mapcar (lambda (pred act) (abs (- pred act)))
                                   predicted actual)))
                    (format t "~15a Iteration: ~5s Predicted: ~25s Actual: ~25s Error: ~25s~%"
                            (funcall number-formatter ,iter-number)
                            ,iter-number
                            predicted actual errors)
                    (dotimes (i (length predicted)) (pop inputs)) ; Pop from head
                    (nconc tail predicted)                        ; Push to tail
                    (setf tail (last predicted)))))               ; Adjust tail reference
      (if iterations
        (loop for i from 1 upto iterations do (body i))
        (loop for i from 1 do (body i))))))

