% CS 471
% N3: Generative Neural Networks
% Nathan Hawrysh, Computer Science

---
header-includes: |
    \usepackage{pifont}
    \newcommand{\ck}{\ding{51}}
    \usepackage{float}
    \usepackage{import}
    \usepackage{xifthen}
    \usepackage{pdfpages}
    \usepackage{transparent}
numbersections: true
---

\
\
\

1. \ck Did you follow all the requirements in implementing the system?
2. \ck Did you create one Word file of your report with the cover page and section headers as specified?
3. \ck Did you answer all questions per section?

\newpage

*This document requires Pandoc Markdown extensions; else it will not render correctly.*

# Analysis of the Problem Space

1.  **Discuss how you will use the provided function to generate trading** [assumed: training] **data**

    I began by defining the given function as an ordinary Lisp function. My neural network system contains a concept of iterators, similar to ones that have become common in languages like Python and Rust. Common Lisp does not have a concept of such iterators by default, though it's simple to create them. A trivial example follows:

    ```lisp
    (defun iter-naturals ()
      (let ((i 0))
        (lambda () (incf i))))

    ;; Using the iterator
    (let (iter (iter-naturals))
      (dotimes (i 3)
        (princ (funcall iter-naturals))
        (princ " ")))
    ```

    I define iterators by using the form `let` to bind variables for iteration state, and then returning a lambda from the body of said `let`. We can wrap this form in a function to conveniently create such iterators as needed. The lambda will close over the iteration variables, and we are free to pass and manipulate the lambda as we please.

    By graphing, I observed that our given function has a period of $2\pi$. I chose to write a general-purpose iterator, `iter-periodic`, that steps through a wavelength of a periodic function by a given interval. In the interest of preserving floating-point precision, I chose to have this iterator wrap back around rather than continue toward $x=\infty$.

    One issue with this scheme is that the network is only exposed to set values of the function. Therefore, I included in the function a keyword parameter, `:period-shift` that allows one to slightly adjust the interval between returned data points. Setting this value to something other than 0.0 can cause the interval between returned values to be slightly larger (or smaller) than an integer fraction of the function's period. I set this value to machine epsilon so that, across all cycles, the iterator would yield nearly every possible floating-point value over the wavelength.

    I then wrote `iter-sinewave`, which uses `iter-periodic` to return successive values of the given function, and `iter-train`, which uses `iter-sinewave` to produce valid training data.

2.  **Graph the function, and indicate your strategy to choose and sample data**

    ![Graph of $\frac{1}{2}(1 + \cos(x)).$](media/function-graph.png)

3.  **Provide the parameters your ANN will be trained on**

    Name                     Value
    -----------------------  ----------------------------------------------------------
    `+num-inputs+`           8
    `*steps-per-period*`     64
    `*target-error*`         0.005
    Network structure        `(list +num-inputs+ 12 6 3 1)`
    Epoch length             512 (equal to `(* +num-inputs+  *steps-per-period*)`)

# Designing the Evaluation

1.  **Diagram how you will modify your trained network to predict an x for f(x) that was not the original training data**

    \begin{figure}[H]
        \centering
        \import{./media/}{diagram.pdf_tex}
        \caption{Generation of a sine wave using a generative neural network accepting 4~inputs. Inputs are treated as a queue. In Iteration~1, the network is given true values $f(x)$ for four values of $x$. The earliest input data point is then popped, and the previous prediction is pushed, for use in Iteration~2. After several iterations, the network is operating solely on its own input data, as illustrated in Iteration~16. Taken together, these successive predictions correspond to points on a network-generated sine wave. The network is able to generate these values of $f(x)$ given only four input points.}
    \end{figure}

2.  **Provide how you will analyze the produced predictions to determine network accuracy**

    For each $x$, I take the absolute linear error between the result of directly running the function against the value computed by the neural network. During a test, which takes place before each epoch, I take the mean of these errors to determine the accuracy of the network.

# Implementation

1.  **Must have functions dedicated to do the following.**
    1.  Generated Data:
        -   `iter-sinewave` (demos/sinewave.lisp:36)
        -   `iter-train` (demos/sinewave.lisp:42)
    2.  Train Network: `train-to-target` (train.lisp:98)
    3.  Predict: `run-net-generative` (network.lisp:129)
2.  **What data structure (type and name) did you use to produce generative results?**

    I used a linked queue. Common Lisp's standard library is relatively small by modern standards, and to my knowledge, it does not contain such a data type. The tools that it does provide, however, are extremely powerful, and so it was simple to fashion one myself using the built-in linked list. This was done by keeping references to the head and tail of the list, popping from the head, pushing to the tail, and then updating the tail reference.

# Testing and Results

1.  **Test your program very thoroughly and make sure the output matches your expectations.**

    \ck

    **Produce a .txt file of your generated results.**

    Please see docs/generative-output.txt, in the same directory as this document.

2.  **Your analysis of the results:**

    Upon training the network to a mean error of 0.05 and running, I found that the network would quickly begin constantly outputting a value around 0.86. I decided to continue training the network, this time to an error of 0.001. I found that the network hit a local maximum of 0.004 and cut the training short. With its improved accuracy, I found that the network remained within 0.02 error over iterations\ $[0, 64]$ (equivalent to the interval\ $[0, \frac{1}{2}\pi]$). Soon after this, however, the network struggles somewhat to keep track of the wave, fluctuating to an error of 0.580 on iteration\ 183. While this occurs slightly after the completion of one wavelength, and my iterator loops after this point, I believe this to be a coincidence, as this looping of the iterator should be fully transparent to the network, and only acts to preserve floating-point precision.

3.  **Did it work as expected?  If not, explain exactly where it failed. (This includes correctness of all the intermediate outputs)**

    My code functions as intended, with the exception that due to constraints of time and program structure, the format of the output file may not be exactly as specified. The network's accuracy deteriorates after the first wavelength; I believe that this could be improved using a larger network structure.
