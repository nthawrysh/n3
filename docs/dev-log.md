I noticed and deeply appreciated that you went out of your way to find
sqlite bindings for Lisp in the assignment sheet, despite my likely
being the only one using the language. I would have loved to have used
it, and certainly would have given more time, but sadly my database is
limited just to consuming the BattleDots schema. I decided, with some
regret, to rough it with Unix tools and a bit of shell multitasking.

Mostly for my own reference, I decided to document my process here.

Splitting apart the csv file:

```sh
i=0
for ch in {A..Z}; do
    rg "^$i," 'A_Z Handwritten Data.csv' > $ch.txt &
    i=$((i+1))
done
```

Separating training and testing data:

```sh
#!/bin/sh
PERCENT=80
for ch in {A..Z}; do
    (
        file=$ch.csv
        ln_train=$(( $(wc -l < all/$file) * $PERCENT / 100 ))
        shuf all/$file | split -d -a 1 -l $ln_train - $ch
        mv ${ch}0 train/$file
        mv ${ch}1 test/$file
    )&
done
```

Normalizing data:

```sh
for ch in {A..Z}; do
    awk -i inplace -F , '{
        printf "((#\\%c) ",$1+65;
        for (i=2; i<=NF; i++) {
            printf "%.17gd0 ",$i/255.0
        };
        print ")"
    }' learn/$ch.csv test/$ch.csv &
done
```
