# Neural Network Usage

The following is equivalent to the example on the assignment page for Part 1:

```lisp
(run-net '(((input 4)) (+ +) (+)) 4.1 5.5 3.3 10.1)
```

Within this system:

-   A neural network is a list of layers.
-   A layer is a list of nodes.
-   A node contains one of the following:
    -   A symbol of a function, e.g. `+`
    -   A valid Lisp form, e.g. `(node->net other-net)`.

If the node is a list, then it is `eval`ed (arguments included)
to obtain a function or symbol. The current inputs are then `apply`ed
to the result. This may be viewed as a form of, or at least analogous
to, currying.

This makes it possible to store data inside the network. For instance,
note that the first layer contains the form `(input 4)`. This node type
merely ensures that the number of inputs equals the expected value; if
so, it passes values along unchanged.

This mechanism can also be used to store weights inside the network.
The node type `neuron` makes use of this feature; it combines a weighted
sum with the sigmoid (AKA logistic) function, $\frac{1}{1 + e^{-x}}$:

```lisp
(defparameter *weighted* '(((neuron 0 1/2) (neuron 2 0)) (neuron 1 1)))
(run-net *weighted* 1 1)
```

We could then randomize weights by walking the network searching for `wsum`
forms, then adjust the arguments with `incf`. We can `print` the entire network
in a format neatly readable to humans, then convert it back into a linked
list using the Lisp reader.

```lisp
(save-net *weighted* "networks/weighted.sexp")
(defparameter *it-lives* (load-net "networks/weighted.sexp"))
*it-lives*
```

It's also possible to embed one network inside of another.

```lisp
(defparameter *composite*
  '(((subnet *net*) (inspect-node (subnet *net*) 'subnet1))
    (+)))

(run-net *composite* 4.1 5.5 3.3 10.1)
```

Until the time of execution, `*composite*` only contains the symbols
of the other networks, not direct references. Therefore, one is free
to modify these networks individually, without fear of modifying the
others. ***Caution:*** When loading such networks from files, care
must be taken to bind them to the same symbols again.

# Lisp Crash Course

I recall you mentioning that you don't have a lot of experience with Lisp,
so following are some examples for your reflection. These are included on
the assumption that they're helpful; if not, no harm in ignoring them.

```lisp
(cons 5 9)             ; Making a pair
(list (+ 1 2) (+ 4 6)) ; Evaluated depth-first!
(cons (/ 1 2)          ; Building a list by hand
      (cons (/ 4 6)
             nil))

(quote x y)        ; Returns arguments, does not evaluate
'(x y)             ; Equivalent to the above
`(1 2 ,(+ 1 2))    ; Quasi-quoting! Comma = unquote

(let ((list '(+ - * /))) ; Local binding
  (print list)
  ;; Calling `list` still works because
  ;; functions and variables live in separate namespaces!
  (print (list 1/4 2/16))
  (print (car list))
  (print (cdr list))
  'this-is-the-result)

(if "Hello!" ; Everything is truthy...
  (princ "True!")
  (princ "False!"))

(not nil) ; ...Except nil.
(list)    ; What's nil, you ask?

;;; &rest collects remaining arguments into a list.
(defun well-rested (&rest args)
  (* 2 23) ; Evaluated, but not returned.
  args)    ; Last form is return value.
(well-rested 1 2) ; ==> (1 2)

;; Macros can evaluate their arguments in any order, or not at all.
(defmacro bitbucket (&body who-cares)
  (princ "Nope."))

(bitbucket (princ "Good day!"))
(macroexpand-1 '(bitbucket (princ "Good day!")))
```
