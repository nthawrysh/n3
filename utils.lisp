(defpackage :n3/utils
  (:use :cl)
  (:export :read-csv-line :make-list-of :multimap :mean))

(in-package :n3/utils)

(defun read-csv-line (&key (stream *standard-input*)
                           (delimiter #\,)
                           prompt)
  "Returns the read list of delimited values from a string."
  (when prompt
    (princ prompt)
    (finish-output))
  (let ((*read-eval* nil))
    (read-from-string ; Returns multiple values
      (concatenate 'string
        "("
        (substitute #\space delimiter
                    (read-line stream))
        ")"))))

(defmacro make-list-of (n form)
  `(loop repeat ,n collecting ,form))

(defun multimap (fun &rest lists)
  "Same as mapcar, except that all values from fun will be returned."
  (apply 'nconc
         (apply 'mapcar
                (lambda (&rest elems)
                  (multiple-value-list (apply fun elems)))
                lists)))

(defun mean (data)
  (assert data (data) "mean: No data given")
  (loop for pt in data
        sum pt into summed
        count pt into npts
        finally (return (/ summed npts))))

