(asdf:defsystem "n3"
  :class :package-inferred-system
  :depends-on (:n3/all))

(asdf:defsystem "n3/demos"
  :class :package-inferred-system
  :pathname "demos"
  :depends-on ("n3/demos/andgate"
               "n3/demos/handwriting"
               "n3/demos/sinewave"))
