(defpackage :n3/demos/sinewave
  (:use :cl :n3 :n3/utils)
  (:export :*steps* :*net-sinewave*
           :train :run))

(in-package :n3/demos/sinewave)

(defparameter *steps-per-period* 64
  "Number of data points to divide a wavelength of our function into.")
(defconstant +num-inputs+ 8
  "Number of inputs to the neural network.
   I suggest making this a factor of *steps-per-period*.
   WARNING: May break existing networks if changed!")

(defun target-fun (x)
  (* (+ (cos x) 1) 1/2))

(defun iter-periodic (function &key (start 0d0) end steps (period-shift 0d0))
  "General iterator for periodic functions.

   When training, I suggest using `:period-shift double-float-epsilon` so that
   this iterator is slightly out-of-phase  relative to (- stop start), to give
   near-complete coverage of the function across all cycles."
  (declare (type function function)
           (type (integer 1) steps))
  (let* ((span-interval (- end start))
         (step-interval (+ (/ span-interval steps)
                           period-shift))
         (step start))
    (lambda ()
      (funcall function
               (if (>= step end)
                 (decf step span-interval)
                 (incf step step-interval))))))

(defun iter-sinewave (&key (period-shift 0d0))
  "An iterator returning single, successive values from target-fun."
  (iter-periodic #'target-fun
                 :end (* 2 pi)
                 :steps *steps-per-period*
                 :period-shift period-shift))

(defun iter-train (steps-input &key (period-shift 0d0))
  "An iterator that converts output from iter-sinewave into valid
   training data. For n=steps-input, this will return a list containing

     ((f(xₙ₊₁)) f(x₁) … f(xₙ)).

   Both input and expected values are pulled from .
   I suggest setting steps-input to a factor of *steps-per-period*, which
   puts us out-of-phase by 1 with target-fun and lets us explore each
   possbile series of its values."
  (let ((iter (iter-sinewave :period-shift period-shift)))
    (lambda ()
      (let ((inputs (make-list-of steps-input (funcall iter)))
            (output (funcall iter)))
        (cons (list output) inputs)))))

(defparameter *net*
  (make-net (list +num-inputs+ 12 6 3 1)))

(defun format-number (iter-number)
  (format nil "(~s)π" (/ iter-number *steps-per-period* 2)))

(defun train ()
  (macrolet ((iter () ; Why copy+paste when you can macro?
               ;; Choose a guaranteed common multiple as our epoch length. This won't
               ;; put us precisely back in phase because `iter-periodic` is itself out-
               ;; of-phase by a minuscule amount. Should be representative nonetheless.
               `(epoch (iter-train
                         +num-inputs+
                         :period-shift double-float-epsilon)
                       (* +num-inputs+ *steps-per-period*))))
    (dotimes (i 3) (train-to-target *net* (iter) (iter)))
    (save-net *net* #p"networks/sinewave.lisp")))

(defun run (&optional iterations)
  (setf *net* (load-net #p"networks/sinewave.lisp"))
  (run-net-generative *net*
                      (iter-sinewave
                        :period-shift double-float-epsilon)
                      iterations
                      #'format-number))
