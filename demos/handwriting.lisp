(defpackage :n3/demos/handwriting
  (:use :cl :n3/network :n3/train)
  (:export
    :*net-a* :*net-b* :*net-c* :*net-d* :*net-e* :*net-az*
    :train-single-letter-recognizer :train-all-recognizers))

(in-package :n3/demos/handwriting)

(defparameter *target-error* 0.1)
(defparameter *learn-path* #p"data/learn/_.csv")
(defparameter *test-path* #p"data/test/_.csv")

(defparameter *capitals*
  '(#\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M
    #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z))

(defun rewind (stream)
  (declare (type stream stream))
  (file-position stream 0)
  stream)

(defun set-minus (a b)
  (loop for elem in a
        unless (find elem b)
        collect elem))

(defun letter-path (letter &key (defaults #p"data/learn/_.csv"))
  (make-pathname
    :name (string letter)
    :defaults defaults))

(defun read-random-line (stream chance)
  (loop (if (< (random 1.0) chance)
          (return (read-line stream))
          (read-line stream))))

(defun iter-all-letters (streams)
  (let ((cur-stream streams))
    (lambda ()
      (prog1
        (let ((chosen-stream (car cur-stream)))
          (read-from-string
            (handler-case (read-random-line chosen-stream 0.2)
              (end-of-file () (read-random-line (rewind chosen-stream) 0.2))))) ; Fine if there's an error on the second try.
        (setf cur-stream
              (or (cdr cur-stream) streams))))))

(defmacro replace-expected (iterator expr)
  (let ((iter (gensym))
        (row (gensym)))
    `(let ((,iter ,iterator))
       (lambda ()
         (let ((,row (funcall ,iter)))
           (car ,row)
           (setf (car ,row) ,expr)
           ,row)))))

(defun iter-fractions (denom)
  "So that we can divide the output range (0, 1) for each letter."
  (declare (type (integer 0 *) denom))
  (let* ((interval (coerce (/ 1 denom) 'double-float))
         (idx 1)
         (cur-frac interval))
    (lambda ()
      (prog1
        (list cur-frac)
        (cond
          ((< idx denom)
           (incf idx)
           (incf cur-frac interval))
          (t
            (setf idx 1)
            (setf cur-frac interval)))))))

(defmacro with-open-letters ((streams-var path letters) &body body)
  `(let ((,streams-var
           (mapcar (lambda (letter)
                     (open (letter-path letter :defaults ,path)))
                   ,letters)))
     (unwind-protect
       (progn ,@body)
       (mapc #'close ,streams-var))))

(defun interleave (target-src not-target-src)
  (let ((target-p nil))
    (lambda ()
      (funcall
        (if (setf target-p (not target-p))
          target-src
          not-target-src)))))


(defmacro with-interleaved-letters ((iter-name path letters) &body body)
  (let ((letter-streams (gensym))
        (not-letter-streams (gensym))
        (frac-iter (gensym)))
    `(with-open-letters (,letter-streams ,path ,letters)
       (with-open-letters (,not-letter-streams ,path
                           (set-minus *capitals* ,letters))
         (let* ((,frac-iter (iter-fractions (length ,letters)))
                (,iter-name
                  (interleave (replace-expected
                                (iter-all-letters ,letter-streams)
                                (funcall ,frac-iter))
                              (replace-expected
                                (iter-all-letters ,not-letter-streams)
                                '(0d0)))))
           ,@body)))))

(defvar *net-a* (make-net '(784 100 10 1)))
(defvar *net-b* (make-net '(784 200 28 14 1)))
(defvar *net-c* (make-net '(784 90 70 1)))
(defvar *net-d* (make-net '(784 500 50 1)))
(defvar *net-e* (make-net '(784 100 200 10 1)))
(defvar *net-az* (make-net '(784 500 28 14 1)))

(defun train-single-letter-recognizer (net letters)
  (with-interleaved-letters (learn *learn-path* letters)
    (with-interleaved-letters (test *test-path* letters)
      (dotimes (_ 3) ; Testing data is randomized, so let's rerun a few times.
        (train-to-target net
                         (epoch learn 100)
                         (epoch test 40)))))
  (save-net
    net (letter-path
          (concatenate 'string letters)
          :defaults "networks/handwriting/_.lisp")))

(defun train-multi-letter-recognizer (net letters)
  (with-open-letters (learn-stream *learn-path* letters)
    (with-open-letters (test-stream *test-path* letters)
      (let* ((len (length letters))
             (learn-frac-iter (iter-fractions len))
             (test-frac-iter (iter-fractions len))
             (learn (epoch
                      (replace-expected
                        (iter-all-letters learn-stream)
                        (funcall learn-frac-iter))
                      100))
             (test (epoch
                     (replace-expected
                       (iter-all-letters test-stream)
                       (funcall test-frac-iter))
                     40)))
        (train-to-target net learn test))
     (save-net
       net (letter-path
             (concatenate 'string letters)
             :defaults "networks/handwriting/_.lisp")))))

(defun train-all-recognizers ()
  (train-single-letter-recognizer *net-a* '(#\A))
  (train-single-letter-recognizer *net-b* '(#\B))
  (train-single-letter-recognizer *net-c* '(#\C))
  (train-single-letter-recognizer *net-d* '(#\D))
  (train-single-letter-recognizer *net-e* '(#\E))
  (train-multi-letter-recognizer *net-az* *capitals*))

#| Test the composite network
(with-interleaved-letters (test #p"data/test/_.csv" '(#\A #\B #\C #\D #\E))
  (dotimes (i 10)
    (let ((row (funcall test)))
      (format t "~s ~s~%"
              (car row)
              (run-net-list *net-composite* (cdr row))))))
|#
