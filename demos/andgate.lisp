(defpackage :n3/demos/andgate
  (:use :cl :n3)
  (:export :*andgate-net* :*train-andgate-net*))

(in-package :n3/demos/andgate)

(defparameter *data-andgate*
 #(((0) 0 0 0 0)
   ((0) 0 0 0 1)
   ((0) 0 0 1 0)
   ((0) 0 0 1 1)
   ((0) 0 1 0 0)
   ((0) 0 1 0 1)
   ((0) 0 1 1 0)
   ((0) 0 1 1 1)
   ((0) 1 0 0 0)
   ((0) 1 0 0 1)
   ((0) 1 0 1 0)
   ((0) 1 0 1 1)
   ((0) 1 1 0 0)
   ((0) 1 1 0 1)
   ((0) 1 1 1 0)
   ((1) 1 1 1 1)))

(defun iter-vec (arr)
  (declare (type array arr))
  (let ((idx 0)
        (len (length arr)))
    (lambda ()
      (cond
       ((< idx len)
        (aref *data-andgate* idx)
        (incf idx))
       (t
         (setf idx 0)
         (error 'end-epoch))))))

(defvar *andgate-net*)

(defun train-andgate-net ()
  (train-to-target (setf *andgate-net* (make-net '(4 2 1)))
                   1/20
                   (iter-vec *data-andgate*)
                   (iter-vec *data-andgate*)))
